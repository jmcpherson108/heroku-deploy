# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.2.1

- patch: Internal maintenance: add bitbucket-pipe-release.

## 1.2.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 1.1.4

- patch: Internal maintenance: change pipe metadata according to new structure

## 1.1.3

- patch: Internal maintenance: Add gitignore secrets.

## 1.1.2

- patch: Update the Readme with a new Atlassian Community link.

## 1.1.1

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 1.1.0

- minor: Added a warning message when new version of the pipe is available

## 1.0.4

- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 1.0.3

- patch: Fix the bug with wait parameter being ignored

## 1.0.2

- patch: Added code style checks

## 1.0.1

- patch: Internal maintenance: update pipes toolkit version.

## 1.0.0

- major: Changed variable WAIT to false by default to reduce the time of pipe execution

## 0.1.2

- patch: Minor documentation updates

## 0.1.1

- patch: Fixed WAIT parameter type conversion

## 0.1.0

- minor: Initial release
- patch: Fixed the build script

